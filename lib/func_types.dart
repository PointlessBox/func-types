/// Provides basic functional types like Option, Either and Result.
library func_types;

export 'src/option.dart';
export 'src/either.dart';
export 'src/result.dart';