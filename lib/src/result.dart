import 'package:func_types/func_types.dart';

/// Represents an operation which might fail.
/// Holds either a value of type [OK] or a value of type [REASON], but never both.
///
/// [Result] can have two states:
///  - ok (represented by the [Ok] class), which contains the value of type [OK]
///  produced by the operation
///  - err (represented by the [Err] class), which can contain the error itself,
///  or something that explains the failure (like an error-message or a status-code).
///
/// It is recommended not to use [Result] to pass around [Exception]s or [Error]s.
/// The purpose of [Result] is to provide more useful information like error-messages
/// or status-codes. If you need to fail fast during development, then it is more
/// appropriate to just raise [Exception]s/[Error]s.
///
/// More info about that can be found in this [blog-post](https://fsharpforfunandprofit.com/posts/against-railway-oriented-programming/#summary-of-the-reasons-not-to-use-result)
/// by Scott Wlaschin.
class Result<OK, REASON> {
  final Either<OK, REASON> _either;

  /// Instantiation outside of this package should only be possible through a
  /// factory function to prevent inheritance.
  Result._(this._either);

  /// Tries to executed [uncertain] and returns [Ok] with result when successful,
  /// else returns [Err] with [reason].
  ///
  /// Use this function if the different types of exceptions/errors can be
  /// ignored, and you just want to return a generic explanation about the
  /// failure.
  ///
  /// If the different exception/error types DO matter, then you should
  /// prefer to use a try-catch block and wrap the [Result] yourself with [Ok] and [Err].
  ///
  /// ```dart
  /// // Bad
  /// Resul.tryOrExplain(() => someNetworkCall(), "Problems with network call");
  ///
  /// // Good
  /// try {
  ///   Ok(someNetworkCall())
  /// } catch (ex) {
  ///   if (ex.statusCode >= 500) {
  ///     Err("There is something wrong with the server")
  ///   } else if (ex.statusCode >= 400)
  ///     Err("Your input was wrong")
  ///   } // else if ...
  /// }
  ///
  /// // Good (other example)
  /// Resul.tryOrExplain(() => userArgs.first, "No arguments were provided");
  /// ```
  static Result<RETURN, REASON> tryOrExplain<RETURN, REASON>(
    RETURN Function() uncertain,
    REASON reason,
  ) {
    try {
      return Ok(uncertain());
    } catch (_) {
      return Err(reason);
    }
  }

  /// Holds the inner ok-value as nullable. Using [Result.getOr] should be preferred.
  ///
  /// This should only be used, for example, when you need to pass a nullable
  /// value to another library which does no accept an [Option]-type.
  OK? get unsafe => _either.leftUnsafe;

  /// Tells whether [this] succeeded.
  bool get isOk => _either.isLeft;

  /// Tells whether [this] failed.
  bool get isErr => _either.isRight;

  /// Calls the matching function and returns its value.
  RETURN match<RETURN>({
    required RETURN Function(OK ok) onOk,
    required RETURN Function(REASON reason) onErr,
  }) =>
      _either.match(onLeft: onOk, onRight: onErr);

  /// Calls [onOk] if [this.isOk] and returns its value in an Option.
  Option<RETURN> ifOk<RETURN>(RETURN Function(OK ok) onOk) =>
      _either.ifLeft(onOk);

  /// Async version of [Result.ifOk].
  Future<Option<RETURN>> ifOkAsync<RETURN>(
          Future<RETURN> Function(OK ok) onOk) async =>
      _either.ifLeftAsync(onOk);

  /// Returns ok value if present, else [fallback].
  OK getOr(OK fallback) => _either.leftOr(fallback);

  /// Returns ok value if present, else value produced by [fallback].
  OK getOrDo(OK Function() fallback) => _either.leftOrDo(fallback);

  /// Async version of [Result.getOrDo].
  Future<OK> getOrDoAsync(Future<OK> Function() fallback) =>
      _either.leftOrDoAsync(fallback);

  /// Returns the ok-value as an [Option].
  Option<OK> asOption() => _either.left;
}

extension ResultIterable<OK, ERR> on Iterable<Result<OK, ERR>> {
  /// Filters all ok-[Result] instances and returns the inner values.
  Iterable<OK> okValues() =>
      // Using the non-null assertion operator is ok because e.isOk
      // made sure that every value is a ok-Result.
      where((element) => element.isOk).map((e) => e.unsafe!);

  /// Filters all err-[Result] instances and returns the inner values.
  Iterable<ERR> errValues() =>
      // Using the non-null assertion operator is ok because e.isErr
      // made sure that every value is a err-Result.
      where((element) => element.isErr).map((e) => e._either.rightUnsafe!);
}

/// Represents an ok-[Result] (a successful operation).
/// [Ok] always contains an [OK] value and never an [REASON] value.
class Ok<OK, REASON> extends Result<OK, REASON> {
  Ok._(OK ok) : super._(Left(ok));

  factory Ok(OK ok) {
    return Ok._(ok);
  }
}

/// Represents an err-[Result] (a failed operation).
/// [Err] always contains an [REASON] value and never an [OK] value.
class Err<OK, REASON> extends Result<OK, REASON> {
  Err._(REASON reason) : super._(Right(reason));

  factory Err(REASON reason) {
    return Err._(reason);
  }
}
