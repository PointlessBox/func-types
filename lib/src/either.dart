import 'package:func_types/func_types.dart';

/// Marks unreachable code. It should be made sure that the code is
/// actually unreachable or it will break the library.
class _Unreachable extends Error {}

/// Represents a union-type of [LEFT] and [RIGHT]. Holds either a [LEFT] value
/// or a [RIGHT] value, but never both.
class Either<LEFT, RIGHT> {
  final Option<LEFT> left;
  final Option<RIGHT> right;

  /// Holds the left value as nullable. Using [Either.leftOr] should be preferred.
  ///
  /// This should only be used, for example, when you need to pass a nullable
  /// value to another library which does no accept an [Option]-type.
  LEFT? get leftUnsafe => left.unsafe;

  /// Holds the right value as nullable. Using [Either.rightOr] should be preferred.
  ///
  /// This should only be used, for example, when you need to pass a nullable
  /// value to another library which does no accept an [Option]-type.
  RIGHT? get rightUnsafe => right.unsafe;

  /// Instantiation outside of this package should only be possible through a
  /// factory function to prevent inheritance.
  Either._(this.left, this.right);

  // Does not need a check for _right.isNone because the constructors are exclusive
  /// Tells whether [this] contains an instance of [LEFT] object.
  bool get isLeft => left.isSome;

  // Does not need a check for _left.isNone because the constructors are exclusive
  /// Tells whether [this] contains an instance of [RIGHT].
  bool get isRight => right.isSome;

  /// Handles both possible states ([Left] and [Right]) of this [Either]-instance by
  /// calling the corresponding [onLeft] or [onRight] function.
  ///
  /// Returns the value returned by the executed function.
  RETURN match<RETURN>({
    required RETURN Function(LEFT left) onLeft,
    required RETURN Function(RIGHT right) onRight,
  }) {
    final leftNullable = left.unsafe;
    final rightNullable = right.unsafe;
    if (leftNullable != null) {
      return onLeft(leftNullable);
    } else if (rightNullable != null) {
      return onRight(rightNullable);
    } else {
      // This branch is unreachable because the constructors of 'class Left'
      // and 'class Right' make sure one of both must be a some-Option at all times.
      throw _Unreachable();
    }
  }

  /// Calls [onLeft] if [Either.isLeft] and returns its value in an [Option].
  Option<RETURN> ifLeft<RETURN>(RETURN Function(LEFT left) onLeft) =>
      left.ifSome(onLeft);

  /// Asynchronously calls [onLeft] if [Either.isLeft] and returns its value in
  /// an [Option].
  Future<Option<RETURN>> ifLeftAsync<RETURN>(
          Future<RETURN> Function(LEFT left) onLeft) async =>
      left.ifSomeAsync(onLeft);

  /// Calls [onRight] if [this.isRight] and returns its value in an Option.
  Option<RETURN> ifRight<RETURN>(RETURN Function(RIGHT right) onRight) =>
      right.ifSome(onRight);

  /// Asynchronously calls [onRight] if [Either.isRight] and returns its value in
  /// an [Option].
  Future<Option<RETURN>> ifRightAsync<RETURN>(
          Future<RETURN> Function(RIGHT right) onRight) async =>
      right.ifSomeAsync(onRight);

  /// Returns left value if present, or [fallback] otherwise.
  LEFT leftOr(LEFT fallback) => left.getOr(fallback);

  /// Returns left value if present, or the value returned by [fallback] otherwise.
  LEFT leftOrDo(LEFT Function() fallback) => left.getOrDo(fallback);

  /// Asynchronously returns left value if present, or the value
  /// returned by [fallback] otherwise.
  Future<LEFT> leftOrDoAsync(Future<LEFT> Function() fallback) =>
      left.getOrDoAsync(fallback);

  /// Returns right value if present, or [fallback] otherwise.
  RIGHT rightOr(RIGHT fallback) => right.getOr(fallback);

  /// Returns right value if present, or the value returned by [fallback] otherwise.
  RIGHT rightOrDo(RIGHT Function() fallback) => right.getOrDo(fallback);

  /// Asynchronously returns right value if present, or the value
  /// returned by [fallback] otherwise.
  Future<RIGHT> rightOrDoAsync(Future<RIGHT> Function() fallback) =>
      right.getOrDoAsync(fallback);
}

/// Extension methods on [Iterable] of [Either].
extension EitherIterable<LEFT, RIGHT> on Iterable<Either<LEFT, RIGHT>> {
  /// Filters all instances of [Left] and maps the inner values.
  Iterable<LEFT> leftValues() =>
      // Using the non-null assertion operator is ok because e.isLeft
      // made sure that every value is a left-Either.
      where((e) => e.isLeft).map((e) => e.leftUnsafe!);

  /// Filters all instances of [Right] and maps the inner values.
  Iterable<RIGHT> rightValues() =>
      // Using the non-null assertion operator is ok because e.isLeft
      // made sure that every value is a left-Either.
      where((e) => e.isRight).map((e) => e.rightUnsafe!);
}


/// Represents an instance of [Either] where the [LEFT] value is present.
class Left<LEFT, RIGHT> extends Either<LEFT, RIGHT> {
  Left._(LEFT value) : super._(Some(value), None());
  factory Left(LEFT value) {
    return Left._(value);
  }
}

/// Represents an instance of [Either] where the [RIGHT] value is present.
class Right<LEFT, RIGHT> extends Either<LEFT, RIGHT> {
  Right._(RIGHT value) : super._(None(), Some(value));
  factory Right(RIGHT value) {
    return Right._(value);
  }
}