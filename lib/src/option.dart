/// Represents a nullable value.
/// Ensures safe handling of nullable values by enforcing the explicit handling
/// of both states of the value.
/// Can have the state of
/// - something (represented by the [Some] class) or
/// - nothing (represented by the [None] class).
class Option<MAYBE> {
  final MAYBE? _value;

  /// Instantiation outside of this package should only be possible through a
  /// factory function to prevent inheritance.
  Option._(this._value);

  /// Creates an Option from a nullable [maybeValue].
  factory Option(MAYBE? maybeValue) {
    return Option._(maybeValue);
  }

  /// Saves resources at runtime because of `const` and prevents inheritance.
  const Option._none() : _value = null;

  /// Holds the inner value as nullable. Using [Option.getOr] should be preferred.
  ///
  /// This should only be used, for example, when you need to pass a nullable
  /// value to another library which does no accept an [Option]-type.
  MAYBE? get unsafe => _value;

  /// Tells whether the inner value is present.
  bool get isSome => _value != null;

  /// Tells whether the inner value is not present.
  bool get isNone => !isSome;

  /// Handles both possible states ([Some] and [None]) of this [Option]-instance by
  /// calling the corresponding [onSome] or [onNone] function.
  ///
  /// Returns the value returned by the executed function.
  RETURN match<RETURN>(
      {required RETURN Function(MAYBE some) onSome,
      required RETURN Function() onNone}) {
    final innerValue = _value;
    return innerValue != null ? onSome(innerValue) : onNone();
  }

  /// Calls the given [onSome] function if [Option.isSome] and returns its
  /// value in a new Option.
  Option<RETURN> ifSome<RETURN>(RETURN Function(MAYBE some) onSome) =>
      Option(match(onSome: onSome, onNone: () => null));

  /// Asynchronously calls the given [onSome] function if [Option.isSome] and
  /// returns its value in a new Option.
  Future<Option<RETURN>> ifSomeAsync<RETURN>(
          Future<RETURN> Function(MAYBE some) onSome) async =>
      Option(await match(onSome: onSome, onNone: () async => null));

  // /// Calls the given [onNone] if this is an instance of [None].
  // Option<RETURN> ifNone<RETURN>(RETURN Function() onNone) =>
  //     Option(match(onSome: (some) => null, onNone: onNone));
  //
  // /// Async version of [Option.ifNone].
  // Future<Option<RETURN>> ifNoneAsync<RETURN>(
  //         Future<RETURN> Function() onNone) async =>
  //     Option(await match(onSome: (some) async => null, onNone: onNone));

  /// Gets the inner value if this is a [Some] instance, or [fallback] otherwise.
  MAYBE getOr(MAYBE fallback) => _value ?? fallback;

  /// Gets the inner value if this is a [Some] instance, or result of [fallback] otherwise.
  MAYBE getOrDo(MAYBE Function() fallback) => _value ?? fallback();

  /// Asynchronously gets the inner value if this is a [Some] instance,
  /// or result of [fallback] otherwise.
  Future<MAYBE> getOrDoAsync(Future<MAYBE> Function() fallback) async =>
      _value ?? await fallback();

  // /// Returns the inner value as nullable. Using [Option.getOr] should be preferred.
  // ///
  // /// This should only be used, for example, when you need to pass a nullable
  // /// value to another library which does no accept an [Option]-type.
  // MAYBE? getUnsafe() => _value;
}

/// Extension methods on [Iterable] of [Options].
extension OptionIterable<MAYBE> on Iterable<Option<MAYBE>> {
  /// Filters all instances of [Some] and maps the inner values.
  Iterable<MAYBE> values() =>
      // element.isSome made sure the non-null assertion operator won't fail
      where((element) => element.isSome).map((e) => e.unsafe!);
}

/// Represents an [Option] where the inner value is present (not null).
class Some<SOME> extends Option<SOME> {
  Some._(SOME some) : super._(some);
  factory Some(SOME some) {
    return Some._(some);
  }
}

/// Represents an [Option] where the inner value is NOT present (null).
class None<NEVER> extends Option<NEVER> {
  const None._none() : super._none();
  factory None() {
    return None._none();
  }
}