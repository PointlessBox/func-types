## 1.0.0-1.0.0

- Removed Option#ifNone, Result#ifErr, Result#errOr, Result#tryCatch, ErrTrace
- Changed Option/Result/Either#getUnsafe to getter. E.g. Option.unsafe
- Added unit tests for public api.
- Added Result#tryOrExplain
- Better docs

## 0.1.1

- Update typos in README.md

## 0.1.0

- Initial version.
