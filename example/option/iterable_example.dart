import 'package:func_types/func_types.dart';

List<Option<int>> manyOptions() {
  return List.of([
    Option(1),
    None(),
  ]);
}

void main() {
  // Map all non-null values
  manyOptions().values().forEach((numVal) => print(numVal));
}
