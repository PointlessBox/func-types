import 'package:func_types/func_types.dart';

// DISCLAIMER: This example wont run unless at least one argument is provided.
// This is to better explain the result/basic_example.dart example.
void main(List<String>? nullableArgs) {
  // Options can have two states: some and none,
  // which represent a non-null value and a null value.

  // There are three ways to instantiate an Option:
  // 1. Option(), which takes a nullable value
  // 2. Some(), which can only take a non-null value
  // 3. None(), which takes no arguments

  // To safely handle a nullable value, just wrap it in an Option instance
  final maybeArgs = Option(nullableArgs);

  // Calling match() enforces the handling of both states of the Option instance.
  // This way of handling optional values helps developers to think about
  // what to do when a value is not present, instead of just ignoring it.
  maybeArgs.match(
      onSome: (args) => print('Those were your arguments: $args.'),
      onNone: () => print('No arguments were provided.'));

  // Sometimes there actually are cases where a null value can be ignored,
  // but instead of being tempted to use the non-null assertion operator like this:
  // final firstArg = nullableArgs!.first <-- OUCH!
  // it is possible to safely handle this case by providing a non-null value
  // with the ifSome() method.
  maybeArgs.ifSome((args) => print('Your first argument is: ${args.first}'));
  // There is an async version too.
  () async {
    await maybeArgs
        .ifSomeAsync((args) async => print('Arguments were provided'));
  }();

  // getOr() retrieves the inner value. Of course this value could be null.
  // In that case getOr() provides the given fallback value.
  maybeArgs.getOr(List.empty());
  // getOrDo() can also use a function to produce a fallback value.
  maybeArgs.getOrDo(() => List.empty());
  // And of course there should be a way to handle async fallback functions.
  () async {
    await maybeArgs.getOrDoAsync(() async => List.empty());
  }();

  // IMPORTANT: Of course there are situations where a developer needs to
  // use a nullable values.
  // Consider the case where a Flutter-Widget renders a given String as a message
  // to the user. When the given String is null, then the widget shows the
  // message 'There is nothing to see here' to the user.
  // This widget might be from an external library.
  // In cases like this there is a way to opt-out of the safety provided by
  // the Option-type. A developer could use the match() method and return a
  // null value in the case of a None-instance.
  final nullableAgain = maybeArgs.match(
      onSome: (args) => args,
      onNone: () => null,
  );
  // This way is quite verbose and annoying to use. That is why this library
  // provides a clear way of retrieving the inner value as nullable.
  final unsafeArgs = maybeArgs.unsafe;
  // This way it is less verbose but clearer to the developer, that this
  // action is less safe and should be used with care.
}
