import 'package:func_types/func_types.dart';

List<Either<int, String>> manyEithers() {
  return List.of([
    Left(1),
    Right("2"),
  ]);
}

void main() {
  // Map all left values
  manyEithers().leftValues().forEach((numVal) => print(numVal));

  // Map all right values
  manyEithers().rightValues().forEach((strVal) => print(strVal));
}
