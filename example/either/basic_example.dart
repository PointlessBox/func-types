
import 'package:func_types/func_types.dart';

void main() {
  // Either is a union type of <LEFT, RIGHT>, and can only contain either a value
  // for left OR right but never both at the same time.

  // There are two ways to instantiate an Either:
  // 1. Left()
  // 2. Right()
  final Either<String, int> num = Left("1");
  // final Either<String, int> num = Right(1); // Instance of Right

  // You can match on both cases with the match() function.
  // It will return the value of the matching branch.
  num.match(
      onLeft: (oneStr) {
        print(oneStr.runtimeType == String);
      },
      onRight: (oneInt) {
        print(oneInt.runtimeType == int);
      }
  );

  // In case the value of RIGHT can be ignored, there is the possibility to
  // match only on the LEFT value.
  num.ifLeft((left) => print(left.runtimeType == String));
  // There is an async version too.
  () {
    num.ifLeftAsync((left) async => print(left.runtimeType == String));
  }();

  // To safely get the LEFT value, a fallback value needs to be provided, just in
  // case 'num' is an instance of Right.
  num.leftOr("2");
  // leftOrDo() does the same but with a function which produces the fallback.
  num.leftOrDo(() => "2");
  // And of course there should be a way to handle async fallback functions.
  () async {
    await num.leftOrDoAsync(() async => "2");
  }();

  // The same cases are covered for Right instances too ...
}
