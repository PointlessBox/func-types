
import 'package:func_types/func_types.dart';

import 'basic_example.dart';

// typedef ExResult<T> = Result<T, ErrTrace>;
//
// List<ExResult<String>> manyResults() {
//   return List.of([
//     Result.tryCatch(() => failing()),
//   ]);
// }
//
// void main() {
//   // Map all ok values
//   manyResults().okValues()
//       .forEach((ok) => print(ok));
//
//   // Map all err values
//   manyResults().errValues()
//       .forEach((err) => print(err));
// }