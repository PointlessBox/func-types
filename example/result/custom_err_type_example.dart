import 'package:func_types/func_types.dart';

import 'basic_example.dart';
//
// void main(List<String> args) {
//   printMsg();
// }
//
// typedef Msg = String;
// typedef MsgErr = Result<String, Msg>;
//
// /// Convert a Result<String, ExTrace> to Result<String, MsgKey>.
// MsgErr convertErrToMsgKey() {
//   return Result.tryCatch(() => args.first).match(
//     onOk: (ok) => Ok(ok),
//     onErr: (_) => Err("Something went wrong!"),
//   );
// }
//
// void printMsg() {
//   final msg = convertErrToMsgKey().match(
//     onOk: (msg) => msg,
//     onErr: (errMsg) => errMsg,
//   );
//   print(msg);
// }