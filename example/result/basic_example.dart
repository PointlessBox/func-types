import 'package:func_types/func_types.dart';

void main(List<String> args) {
  // In option/basic_example.dart there is a bug which raises a StateError when
  // no argument is provided to the main function. It is caused by calling
  // args.first on an empty List or arguments.
  // The Result type can be used to safely call args.first without the need to worry
  // about a thrown error or exception. It has two states: Ok and Err, which
  // represent a successful or failed operation.
  // Results help by enforcing a safer handling of operations which might fail.

  // There are three ways to instantiate a Result:
  // 1. Result.tryOrExplain(), which takes a callback which might throw
  // 2. Ok(), which takes the value of a successful operation
  // 3. Err(), which takes the value of a failed operation

  // To safely wrap an uncertain operation, just pass it to Result.tryOrExplain
  final stringResult = Result.tryOrExplain(() => args.first, "No argument provided");

  // ### WHEN NOT TO USE RESULTS ###
  // The Result type is not meant to prevent the use of Exceptions and
  // Errors. There are cases when you want to fail fast. In such cases it is ok
  // to use Exceptions and Errors.
  // More on that topic here:
  // https://fsharpforfunandprofit.com/posts/against-railway-oriented-programming/#summary-of-the-reasons-not-to-use-result
  // ###############################

  // Calling match() enforces the handling of both states of the Result instance.
  // This way of handling uncertain operations helps developers to think about
  // what to do when an operation fails, instead of just ignoring it.
  stringResult.match(
    onOk: (firstArg) => print('Your first argument is: $firstArg'),
    onErr: (reason) => print(reason),
  );

  // It is possible to ignore a failed Result by only matching the Ok-case.
  stringResult.ifOk((str) => print(str));
  // There is an async version too.
  () async {
    await stringResult.ifOkAsync((str) async => print(str));
  }();

  // To retrieve the inner value safely. A fallback value needs to be provided for
  // the case of a failed operation.
  stringResult.getOr("2");
  // getOrDo() can also use a function to produce a fallback value.
  stringResult.getOrDo(() => "2");
  // And of course there should be a way to handle async fallback functions.
  () async {
    await stringResult.getOrDoAsync(() async => "2");
  }();
}