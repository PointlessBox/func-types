Provides functional types for dart.

Why should I use this library?  
There are many libraries providing functional types, but they provide to many ways
to opt-out of the safe handling of values. This library tries to enforce safe
handling of values by clearly marking error-prone actions as 'unsafe'.

### Unsafe

In the context of this library **unsafe means operating on a nullable value**.

## IMPORTANT

### This is a pre-release version and the public api might change.

## Inspiration

This library is inspired by the Enum-Types of the [Rust](https://www.rust-lang.org/) programming language as well as
the practice of '[railway oriented programming](https://fsharpforfunandprofit.com/rop/)'.

## Features

Provides the following types:

- [x] Option
- [x] Either
- [x] Result

Also provides extension-functions on Iterables of the above types.

## Getting started

More examples are in the example folder of the git repository.

## Usage

### Option

```dart
// DISCLAIMER: This example wont run unless at least one argument is provided.
// This is to better explain the result/basic_example.dart example.
void main(List<String>? nullableArgs) {
  // Options can have two states: some and none,
  // which represent a non-null value and a null value.

  // There are three ways to instantiate an Option:
  // 1. Option(), which takes a nullable value
  // 2. Some(), which can only take a non-null value
  // 3. None(), which takes no arguments

  // To safely handle a nullable value, just wrap it in an Option instance
  final maybeArgs = Option(nullableArgs);

  // Calling match() enforces the handling of both states of the Option instance.
  // This way of handling optional values helps developers to think about
  // what to do when a value is not present, instead of just ignoring it.
  maybeArgs.match(
      onSome: (args) => print('Those were your arguments: $args.'),
      onNone: () => print('No arguments were provided.'));

  // Sometimes there actually are cases where a null value can be ignored,
  // but instead of being tempted to use the non-null assertion operator like this:
  // final firstArg = nullableArgs!.first <-- OUCH!
  // it is possible to safely handle this case by providing a non-null value
  // with the ifSome() method.
  maybeArgs.ifSome((args) => print('Your first argument is: ${args.first}'));
  // There is an async version too.
  () async {
    await maybeArgs
        .ifSomeAsync((args) async => print('Arguments were provided'));
  }();

  // getOr() retrieves the inner value. Of course this value could be null.
  // In that case getOr() provides the given fallback value.
  maybeArgs.getOr(List.empty());
  // getOrDo() can also use a function to produce a fallback value.
  maybeArgs.getOrDo(() => List.empty());
  // And of course there should be a way to handle async fallback functions.
  () async {
    await maybeArgs.getOrDoAsync(() async => List.empty());
  }();

  // IMPORTANT: Of course there are situations where a developer needs to
  // use a nullable values.
  // Consider the case where a Flutter-Widget renders a given String as a message
  // to the user. When the given String is null, then the widget shows the
  // message 'There is nothing to see here' to the user.
  // This widget might be from an external library.
  // In cases like this there is a way to opt-out of the safety provided by
  // the Option-type. A developer could use the match() method and return a
  // null value in the case of a None-instance.
  final nullableAgain = maybeArgs.match(
    onSome: (args) => args,
    onNone: () => null,
  );
  // This way is quite verbose and annoying to use. That is why this library
  // provides a clear way of retrieving the inner value as nullable.
  final unsafeArgs = maybeArgs.unsafe;
  // This way it is less verbose but clearer to the developer, that this
  // action is less safe and should be used with care.
}
```

### Either

```dart
void main() {
  // Either is a union type of <LEFT, RIGHT>, and can only contain either a value
  // for left OR right but never both at the same time.

  // There are two ways to instantiate an Either:
  // 1. Left()
  // 2. Right()
  final Either<String, int> num = Left("1");
  // final Either<String, int> num = Right(1); // Instance of Right

  // You can match on both cases with the match() function.
  // It will return the value of the matching branch.
  num.match(
      onLeft: (oneStr) {
        print(oneStr.runtimeType == String);
      },
      onRight: (oneInt) {
        print(oneInt.runtimeType == int);
      }
  );

  // In case the value of RIGHT can be ignored, there is the possibility to
  // match only on the LEFT value.
  num.ifLeft((left) => print(left.runtimeType == String));
  // There is an async version too.
  () {
    num.ifLeftAsync((left) async => print(left.runtimeType == String));
  }();

  // To safely get the LEFT value, a fallback value needs to be provided, just in
  // case 'num' is an instance of Right.
  num.leftOr("2");
  // leftOrDo() does the same but with a function which produces the fallback.
  num.leftOrDo(() => "2");
  // And of course there should be a way to handle async fallback functions.
  () async {
    await num.leftOrDoAsync(() async => "2");
  }();

  // The same cases are covered for Right instances too ...
}
```

### Result

#### When to use

There is a
good [blog-post](https://fsharpforfunandprofit.com/posts/against-railway-oriented-programming/#summary-of-the-reasons-not-to-use-result)
by Scott Wlaschin about when and when not to use the Result type.

```dart
void main(List<String> args) {
  // In option/basic_example.dart there is a bug which raises a StateError when
  // no argument is provided to the main function. It is caused by calling
  // args.first on an empty List or arguments.
  // The Result type can be used to safely call args.first without the need to worry
  // about a thrown error or exception. It has two states: Ok and Err, which
  // represent a successful or failed operation.
  // Results help by enforcing a safer handling of operations which might fail.

  // There are three ways to instantiate a Result:
  // 1. Result.tryOrExplain(), which takes a callback which might throw
  // 2. Ok(), which takes the value of a successful operation
  // 3. Err(), which takes the value of a failed operation

  // To safely wrap an uncertain operation, just pass it to Result.tryOrExplain
  final stringResult = Result.tryOrExplain(() => args.first, "No argument provided");

  // ### WHEN NOT TO USE RESULTS ###
  // The Result type is not meant to prevent the use of Exceptions and
  // Errors. There are cases when you want to fail fast. In such cases it is ok
  // to use Exceptions and Errors.
  // More on that topic here:
  // https://fsharpforfunandprofit.com/posts/against-railway-oriented-programming/#summary-of-the-reasons-not-to-use-result
  // ###############################

  // Calling match() enforces the handling of both states of the Result instance.
  // This way of handling uncertain operations helps developers to think about
  // what to do when an operation fails, instead of just ignoring it.
  stringResult.match(
    onOk: (firstArg) => print('Your first argument is: $firstArg'),
    onErr: (reason) => print(reason),
  );

  // It is possible to ignore a failed Result by only matching the Ok-case.
  stringResult.ifOk((str) => print(str));
  // There is an async version too.
  () async {
    await stringResult.ifOkAsync((str) async => print(str));
  }();

  // To retrieve the inner value safely. A fallback value needs to be provided for
  // the case of a failed operation.
  stringResult.getOr("2");
  // getOrDo() can also use a function to produce a fallback value.
  stringResult.getOrDo(() => "2");
  // And of course there should be a way to handle async fallback functions.
  () async {
    await stringResult.getOrDoAsync(() async => "2");
  }();
}
```
