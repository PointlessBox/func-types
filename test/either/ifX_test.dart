
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Either.ifLeft()', () {

    test("should call the given block if it's a Left-Either and return its value in a new Option", () {
      final maybeResult = left.ifLeft((left) {
        return left;
      });
      final result = maybeResult.match(
          onSome: (some) {
            return some;
          },
          onNone: () {
            return "";
          }
      );
      expect(maybeResult.isSome, isTrue);
      expect(result, leftInner);
    });

    test("should NOT call the given block if it's a Right-Either and return a new None-Option", () {
      final maybeResult = right.ifLeft((left) {
        return left;
      });
      expect(maybeResult.isNone, isTrue);
    });
  });

  group('Either.ifRight()', () {

    test("should call the given block if it's a Right-Either and return its value in a new Option", () {
      final maybeResult = right.ifRight((right) {
        return right;
      });
      final result = maybeResult.match(
          onSome: (some) {
            return some;
          },
          onNone: () {
            return "";
          }
      );
      expect(maybeResult.isSome, isTrue);
      expect(result, rightInner);
    });

    test("should NOT call the given block if it's a Left-Either and return a new None-Option", () {
      final maybeResult = left.ifRight((right) {
        return right;
      });
      expect(maybeResult.isNone, isTrue);
    });
  });
}