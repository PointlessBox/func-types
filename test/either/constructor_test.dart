import 'package:func_types/func_types.dart';
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Left()', () {
    test('should produce a left-Either', () {
      final left = Left(1);
      expect(left.isLeft, isTrue);
      expect(left.isRight, isFalse);
    });
  });

  group('Right()', () {
    test('should produce a left-Either', () {
      final right = Right(1);
      expect(right.isLeft, isFalse);
      expect(right.isRight, isTrue);
    });
  });
}