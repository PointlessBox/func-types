import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Either.match()', () {
    test('should call the onLeft function if it is a left-Either', () {
      final wasLeft = left.match(
        onLeft: (leftValue) {
          return true;
        },
        onRight: (rightValue) {
          return false;
        }
      );
      expect(wasLeft, isTrue);
    });

    test('should call the onLeft function if it is a left-Either', () {
      final wasRight = right.match(
          onLeft: (leftValue) {
            return false;
          },
          onRight: (rightValue) {
            return true;
          }
      );
      expect(wasRight, isTrue);
    });

    test('should be able to handle async functions', () async {
      final wasRight = await right.match(
          onLeft: (leftValue) async {
            return false;
          },
          onRight: (rightValue) async {
            return true;
          }
      );
      expect(wasRight, isTrue);

      final wasLeft = await left.match(
          onLeft: (leftValue) async {
            return true;
          },
          onRight: (rightValue) async {
            return false;
          }
      );
      expect(wasLeft, isTrue);
    });
  });
}