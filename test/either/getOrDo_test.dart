
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {

  group('Either.leftOrDo()', () {

    test("should return the left value if it's a Left-Either", () {
      final result = left.leftOrDo(fallbackFunc);
      expect(result, leftInner);
    });

    test("should return the fallback value if it's a Right-Either", () {
      final result = right.leftOrDo(fallbackFunc);
      expect(result, fallbackValue);
    });
  });

  group('Either.rightOrDo()', () {

    test("should return the right value if it's a Right-Either", () {
      final result = right.rightOrDo(fallbackFunc);
      expect(result, rightInner);
    });

    test("should return the fallback value if it's a Left-Either", () {
      final result = left.rightOrDo(fallbackFunc);
      expect(result, fallbackValue);
    });
  });
}