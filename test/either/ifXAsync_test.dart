
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Either.ifLeftAsync()', () {

    test("should call the given block if it's a Left-Either and return its value in a new Option", () async {
      final maybeResult = await left.ifLeftAsync((left) async {
        return left;
      });
      final result = maybeResult.match(
          onSome: (some) {
            return some;
          },
          onNone: () {
            return "";
          }
      );
      expect(maybeResult.isSome, isTrue);
      expect(result, leftInner);
    });

    test("should NOT call the given block if it's a Right-Either and return a new None-Option", () async {
      final maybeResult = await right.ifLeftAsync((left) async {
        return left;
      });
      expect(maybeResult.isNone, isTrue);
    });
  });

  group('Either.ifRightAsync()', () {

    test("should call the given block if it's a Right-Either and return its value in a new Option", () async {
      final maybeResult = await right.ifRightAsync((right) async {
        return right;
      });
      final result = maybeResult.match(
          onSome: (some) {
            return some;
          },
          onNone: () {
            return "";
          }
      );
      expect(maybeResult.isSome, isTrue);
      expect(result, rightInner);
    });

    test("should NOT call the given block if it's a Left-Either and return a new None-Option", () async {
      final maybeResult = await left.ifRightAsync((right) async {
        return right;
      });
      expect(maybeResult.isNone, isTrue);
    });
  });
}