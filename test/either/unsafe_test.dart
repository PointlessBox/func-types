import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Either.leftUnsafe', () {
    test('should return a nullable value which is not null if called on an Left-Either', () {
      final nullable = left.leftUnsafe;
      expect(nullable, leftInner);
    });

    test('should return null if called on an Right-Either', () {
      final nullable = right.leftUnsafe;
      expect(nullable, null);
    });
  });

  group('Either.rightUnsafe', () {
    test('should return a nullable value which is not null if called on an Right-Either', () {
      final nullable = right.rightUnsafe;
      expect(nullable, rightInner);
    });

    test('should return null if called on an Left-Either', () {
      final nullable = right.leftUnsafe;
      expect(nullable, null);
    });
  });
}
