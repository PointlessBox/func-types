
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {

  group('Either.leftOr()', () {

    test("should return the left value if it's a Left-Either", () {
      final result = left.leftOr(fallbackValue);
      expect(result, leftInner);
    });

    test("should return the fallback value if it's a Right-Either", () {
      final result = right.leftOr(fallbackValue);
      expect(result, fallbackValue);
    });
  });

  group('Either.rightOr()', () {

    test("should return the right value if it's a Right-Either", () {
      final result = right.rightOr(fallbackValue);
      expect(result, rightInner);
    });

    test("should return the fallback value if it's a Left-Either", () {
      final result = left.rightOr(fallbackValue);
      expect(result, fallbackValue);
    });
  });
}