
import 'package:func_types/func_types.dart';

final leftInner = "LEFT";
final rightInner = "RIGHT";
final fallbackValue = "FALLBACK";
String fallbackFunc() => "FALLBACK";
Future<String> fallbackFuncAsync() async => "FALLBACK";
final Either<String, String> left = Left(leftInner);
final Either<String, String> right = Right(rightInner);
final leftValues = List.of([
  left,
  left,
]);
final List<Either<String, String>> rightValues = List.of([
  right,
]);
final listOfEither = List.of(leftValues)..addAll(rightValues);