import 'package:func_types/func_types.dart';
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Iterable<Either>.leftValues()', () {
    test('should return only left-Either values', () {
      final filtered = listOfEither.leftValues();
      for (var element in filtered) {
        expect(element, leftInner);
      }
      expect(filtered.length, leftValues.length);
    });
  });
  group('Iterable<Either>.rightValues()', () {
    test('should return only left-Either values', () {
      final filtered = listOfEither.rightValues();
      for (var element in filtered) {
        expect(element, rightInner);
      }
      expect(filtered.length, rightValues.length);
    });
  });
}