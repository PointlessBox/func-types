
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {

  group('Either.leftOrDoAsync()', () {

    test("should return the left value if it's a Left-Either", () async {
      final result = await left.leftOrDoAsync(fallbackFuncAsync);
      expect(result, leftInner);
    });

    test("should return the fallback value if it's a Right-Either", () async {
      final result = await right.leftOrDoAsync(fallbackFuncAsync);
      expect(result, fallbackValue);
    });
  });

  group('Either.rightOrDoAsync()', () {

    test("should return the right value if it's a Right-Either", () async {
      final result = await right.rightOrDoAsync(fallbackFuncAsync);
      expect(result, rightInner);
    });

    test("should return the fallback value if it's a Left-Either", () async {
      final result = await left.rightOrDoAsync(fallbackFuncAsync);
      expect(result, fallbackValue);
    });
  });
}