
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Option.ifSome()', () {

    test("should call the given block if it's a some-option and return its value in a new Option", () async {
      final maybeResult = await someValue.ifSomeAsync((some) async {
        return some;
      });
      final result = maybeResult.match(
          onSome: (some) {
            return some;
          },
          onNone: () {
            return onNoneReturnValue;
          }
      );
      expect(maybeResult.isSome, isTrue);
      expect(result, innerValue);
    });

    test("should NOT call the given block if it's a none-option and return a new none-Option", () async {
      final maybeResult = await noneValue.ifSomeAsync((some) async {
        return some;
      });
      expect(maybeResult.isNone, isTrue);
    });
  });
}