
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Option.getOrDoAsync()', () {
    test("should return the inner value if it's a some-option", () async {
      final result = await someValue.getOrDoAsync(fallbackFuncAsync);
      expect(result, innerValue);
    });

    test(
        "should return the value of the fallback function if it's a none-option",
        () async {
      final result = await noneValue.getOrDoAsync(fallbackFuncAsync);
      expect(result, fallbackValue);
    });
  });
}
