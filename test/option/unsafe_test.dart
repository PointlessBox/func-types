import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Option.unsafe', () {
    test('should return a nullable value which is not null if called on an Some-Option', () {
      final nullable = someValue.unsafe;
      expect(nullable, innerValue);
    });

    test('should return null if called on an None-Option', () {
      final nullable = noneValue.unsafe;
      expect(nullable, null);
    });
  });
}
