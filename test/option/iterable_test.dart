import 'package:test/test.dart';
import 'package:func_types/func_types.dart';

import 'default_setup.dart';

void main() {
  group('Iterable<Option>.values()', () {
    test('should return non-null values', () {
      final filtered = listOfOptions.values();
      for (var element in filtered) {
        expect(element, innerValue);
      }
      expect(filtered.length, someValues.length);
    });
  });
}