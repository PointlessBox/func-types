import 'package:func_types/func_types.dart';
import 'package:test/test.dart';

void main() {
  group('Option()', () {

    test('should be something if called with a non-null value', () {
      final someValue = Option(1);
      expect(someValue.isSome, isTrue);
      expect(someValue.isNone, isFalse);
    });
    test('should be nothing if called with a null value', () {
      final noneValue = Option(null);
      expect(noneValue.isSome, isFalse);
      expect(noneValue.isNone, isTrue);
    });
  });

  group('Some()', () {

    test('should be a none-option with', () {
      final someValue = Some(1);
      expect(someValue.isSome, isTrue);
      expect(someValue.isNone, isFalse);
    });
  });

  group('None()', () {

    test('should be a none-option with', () {
      final noneValue = None();
      expect(noneValue.isNone, isTrue);
      expect(noneValue.isSome, isFalse);
    });
  });
}
