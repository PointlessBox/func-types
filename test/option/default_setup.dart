import 'package:func_types/func_types.dart';

final innerValue = "SOME";
final someValue = Option(innerValue);
final noneValue = None<String>();
final fallbackValue = "FALLBACK";
final someValues = List.of([
  someValue,
  someValue,
]);
final List<Option<String>> noneValues = List.of([
  noneValue,
]);
final listOfOptions = List.of(someValues)..addAll(noneValues);

final onNoneReturnValue = "NONE";

String fallbackFunc() => fallbackValue;
Future<String> fallbackFuncAsync() async => fallbackValue;
