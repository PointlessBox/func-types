
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {

  group('Option.getOr()', () {

    test("should return the inner value if it's a some-option", () {
      final result = someValue.getOr(fallbackValue);
      expect(result, innerValue);
    });

    test("should return the fallback value if it's a none-option", () {
      final result = noneValue.getOr(fallbackValue);
      expect(result, fallbackValue);
    });
  });
}