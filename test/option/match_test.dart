
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Option.match()', () {

    test('should call the onSome block and return its returned value if called on a some-option', () {
      final result = someValue.match(
          onSome: (some) {
            return some;
          },
          onNone: () {
            return onNoneReturnValue;
          }
      );
      expect(result, innerValue);
    });

    test('should call the onNone block and return its returned value if called on a some-option', () {
      final result = noneValue.match(
          onSome: (some) {
            return some;
          },
          onNone: () {
            return onNoneReturnValue;
          }
      );
      expect(result, onNoneReturnValue);
    });

    test('be able to handle async functions', () async {
      final noneResult = await noneValue.match(
          onSome: (some) async {
            return some;
          },
          onNone: () async {
            return onNoneReturnValue;
          }
      );
      expect(noneResult, onNoneReturnValue);

      final someResult = await someValue.match(
          onSome: (some) async {
            return some;
          },
          onNone: () async {
            return onNoneReturnValue;
          }
      );
      expect(someResult, innerValue);
    });
  });
}