
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {

  group('Option.getOrDo()', () {

    test("should return the inner value if it's a some-option", () {
      final result = someValue.getOrDo(fallbackFunc);
      expect(result, innerValue);
    });

    test("should return the value of the fallback function if it's a none-option", () {
      final result = noneValue.getOrDo(fallbackFunc);
      expect(result, fallbackValue);
    });
  });
}