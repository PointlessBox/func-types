import 'package:func_types/func_types.dart';
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Iterable<Result>.okValues()', () {
    test('should only return ok-values', () {
      final filtered = listOfResults.okValues();
      for (var element in filtered) {
        expect(element, okValue);
      }
      expect(filtered.length, okResults.length);
    });
  });

  group('Iterable<Result>.errValues()', () {
    test('should only return err-values', () {
      final filtered = listOfResults.errValues();
      for (var element in filtered) {
        expect(element, errReason);
      }
      expect(filtered.length, errResults.length);
    });
  });
}
