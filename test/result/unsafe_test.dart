import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Result.unsafe', () {
    test('should return a nullable value which is not null if called on an Ok-Result', () {
      final nullable = okResult.unsafe;
      expect(nullable, okValue);
    });

    test('should return null if called on an Err-Result', () {
      final nullable = errResult.unsafe;
      expect(nullable, null);
    });
  });
}
