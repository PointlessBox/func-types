import 'package:func_types/func_types.dart';
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Result.tryOrExplain()', () {
    test('should produce an ok-Result on a succeeding operation', () {
      final numberResult = Result.tryOrExplain(() => 1, errReason);
      expect(numberResult.isOk, isTrue);
      expect(numberResult.isErr, isFalse);
    });

    test('should produce an err-Result on a failing operation', () {
      final numberResult = Result.tryOrExplain(() => throw Exception(), errReason);
      expect(numberResult.isOk, isFalse);
      expect(numberResult.isErr, isTrue);
    });
  });

  group('Ok()', () {
    test('should produce an Ok-Result', () {
      final numberResult = Ok(1);
      expect(numberResult.isOk, isTrue);
      expect(numberResult.isErr, isFalse);
    });
  });

  group('Err()', () {
    test('should produce an Err-Result', () {
      final numberResult = Err("This failed");
      expect(numberResult.isErr, isTrue);
      expect(numberResult.isOk, isFalse);
    });
  });
}
