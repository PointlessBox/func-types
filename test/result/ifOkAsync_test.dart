
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Result.ifOkAsync()', () {
    test('should call the given function when called on an Ok-Result', () async {
      final wasCalled = await okResult.ifOkAsync((_) async => "");
      expect(wasCalled.isSome, isTrue);
    });

    test('should NOT call the given function when called on an Err-Result', () async {
      final wasCalled = await errResult.ifOkAsync((_) async => "");
      expect(wasCalled.isSome, isFalse);
    });
  });
}
