
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Result.asOption()', () {

    test("should transform a Result into an Option where the ok-value turns into the some-value", () async {
      final okResultOption = okResult.asOption();
      final errResultOption = errResult.asOption();
      expect(okResultOption.isSome, isTrue);
      expect(errResultOption.isSome, isFalse);
    });
  });
}
