
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {

  group('Result.getOrDo()', () {

    test("should return the inner value if it's a Ok-Result", () {
      final result = okResult.getOrDo(() => fallbackValue);
      expect(result, okValue);
    });

    test("should return the fallback value if it's a Err-Result", () {
      final result = errResult.getOrDo(() => fallbackValue);
      expect(result, fallbackValue);
    });
  });
}