
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Result.getOrDoAsync()', () {

    test("should return the inner value if it's a Ok-Result", () async {
      final result = await okResult.getOrDoAsync(() async => fallbackValue);
      expect(result, okValue);
    });

    test("should return the fallback value if it's a Err-Result", () async {
      final result = await errResult.getOrDoAsync(() async => fallbackValue);
      expect(result, fallbackValue);
    });
  });
}
