
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Result.ifOk()', () {
    test('should call the given function when called on an Ok-Result', () {
      final wasCalled = okResult.ifOk((_) => "");
      expect(wasCalled.isSome, isTrue);
    });

    test('should NOT the given function when called on an Err-Result', () {
      final wasCalled = errResult.ifOk((_) => "");
      expect(wasCalled.isSome, isFalse);
    });
  });
}
