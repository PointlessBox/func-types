import 'package:test/test.dart';

import 'default_setup.dart';

void main() {
  group('Result.match()', () {
    test('should call the corresponding function and return the value of the called function', () {
      final onOkCalled = okResult.match(onOk: (_) => true, onErr: (_) => false);
      final onErrCalled = errResult.match(onOk: (_) => false, onErr: (_) => true);
      expect(onOkCalled, isTrue);
      expect(onErrCalled, isTrue);
    });
  });
}
