
import 'package:test/test.dart';

import 'default_setup.dart';

void main() {

  group('Result.getOr()', () {

    test("should return the inner value if it's a Ok-Result", () {
      final result = okResult.getOr(fallbackValue);
      expect(result, okValue);
    });

    test("should return the fallback value if it's a Err-Result", () {
      final result = errResult.getOr(fallbackValue);
      expect(result, fallbackValue);
    });
  });
}