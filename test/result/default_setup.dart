import 'package:func_types/func_types.dart';

final okValue = "OK";
final errReason = "Something went wrong";
final fallbackValue = "FALLBACK";
final Result<String, String> okResult = Ok(okValue);
final Result<String, String> errResult = Err(errReason);
final okResults = List.of([
  okResult,
  okResult,
]);
final errResults = List.of([
  errResult,
]);
final listOfResults = List.of(okResults)..addAll(errResults);
